const sql = require("./db.js");

const Feedback = function(feedback) {
	this.NAME = feedback.NAME;
	this.USER_ID = feedback.USER_ID;
	this.AGE = feedback.AGE;
	this.FEEDBACK = feedback.FEEDBACK;
	
};

Feedback.postRequest = (USER_ID, result) => {

	let query = 'CALL users_givefeedback(?)';
	let data = [USER_id];

	sql.query(query, data, (err, res) => {
		if(err) {
    		console.log("error: ", err);
      		result(err, null);
      		return;
    	}
    	console.log("result from sql in loginModel(password_retrieval) = ",res[0][0].result);
    	result(null, res[0][0].result);
	});
};

Login.getRequest = (USER_ID, result) => {
	let query = 'CALL login_users(?)';
	let data = [USER_id];

	sql.query(query, data, (err, res) => {
		if(err) {
			result(err, null);
			return;
		}
		console.log("result from sql in loginModel(login_users) = ",res[0][0]);
		result(null, res[0][0]);
	});
};

module.exports = Feedback;