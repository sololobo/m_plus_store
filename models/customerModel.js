const sql = require("./db.js");

const Customer = function(customer) {
	this.first_name = customer.first_name;
	this.last_name = customer.last_name;
	this.date_of_birth = customer.date_of_birth;
	this.gender = customer.gender;
	this.email_id = customer.email_id;
	this.pwd = customer.pwd;
	this.addr_line_1 = customer.addr_line_1;
	this.addr_line_2 = customer.addr_line_2;
	this.city = customer.city;
	this.state = customer.state;
	this.country = customer.country;
	this.pincode = customer.pincode;

};

Customer.create = (newCustomer, result) => {

 	let insertQuery = 'CALL patient_registration(?,?,?,?,?,?,?,?,?,?,?,?)';
 	let data = [newCustomer.first_name, newCustomer.last_name, newCustomer.date_of_birth, newCustomer.gender,
             newCustomer.email_id, newCustomer.pwd,newCustomer.addr_line_1, newCustomer.addr_line_2, newCustomer.city,
              newCustomer.state, newCustomer.country, newCustomer.pincode];

    sql.query(insertQuery, data, (err, res) => {
    	if(err) {
    		console.log("error: ", err);
      		result(err, null);
      		return;
    	}

    	result(null, { id:  res[0][0]["result"]});
    });
};

Customer.getAll = result => {
	console.log("Customer model page, getAll");
	let query = 'CALL get_all_patients()';
	sql.query(query, (err, res) => {
		if(err) {
			console.log("error: ", err);
      		result(err, null);
      		return;
		}
		console.log("result from sql = ",res);
		result(null, res[0]);
	});
};

Customer.findById = (customerId, result) => {

	let query = 'CALL view_Patient_profile(?)';
	let data = customerId;
	sql.query(query, data, (err, res) => {
		if(err) {
			console.log("error: ", err);
      		result(err, null);
      		return;
		}

		if(res.length) {
			result(null, res[0]);
			return;
		}

		result({ kind: "not_found" }, null);
	});
};

Customer.updateById = (id, customer, result) => {

	let query = 'CALL update_patient_details(?,?,?,?,?,?,?,?,?)';
	let data = [id, customer.date_of_birth, customer.gender, customer.addr_line_1, customer.addr_line_2, customer.city, customer.state, customer.country, customer.pincode];
	sql.query(query, data, (err, res) => {
			if (err) {
		        console.log("error: ", err);
		        result(null, err);
		        return;
		  	}

		  	if(res.affectedRows == 0) {
		  		result({ kind: "not_found" }, null);
		  		return;
		  	}
		  	result(null, {id:id, ...customer});
		}
	);
};

Customer.remove = (id, result) => {

	let query = 'CALL delete_patient_Details(?)';
	let data = id;
	sql.query(query, data, (err, res) => {
		if(err) {
			console.log("error: ", err);
      		result(err, null);
      		return;
		}

		if(res.affectedRows == 0) {
			result({ kind: "not_found" }, null);
			return;
		}

		result(null, res);
	});
};

module.exports = Customer;
