const sql = require("./db.js");

// constructor
const Order = function(order) {
  this.date_of_order = order.date_of_order;
  this.total_price = order.total_price;
  this.isCOD = order.isCOD;
  this.medicines = order.medicines;
  this.prescription = order.prescription;
  this.addressFlag=order.addressFlag;
  this.address_line_1 = order.address_line_1;
  this.address_line_2 = order.address_line_2;
  this.city = order.city;
  this.state = order.state;
  this.country = order.country;
  this.pincode = order.pincode;
};

Order.create = (newOrder, result) => {
  let insertionQuery,data;
  //let str="Call m_plus_store.";
  if(newOrder.addressFlag)
  {
    insertionQuery="Call patient_order_with_addressID(?,?,?,?,?)";
    data=[newOrder.date_of_order,newOrder.total_price,newOrder.isCOD,11,6];

  }
  else
  {
    insertionQuery = "Call patient_order_without_addressID(?, ?, ?, ?, ?,?,?,?,?,?)";
    data = [newOrder.date_of_order,newOrder.total_price,newOrder.isCOD,newOrder.address_line_1, newOrder.address_line_2, newOrder.city, newOrder.state, newOrder.country,
   newOrder.pincode,6];
  }

  sql.query(insertionQuery, data, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    //console.log("json from db is ",res[0][0].result);
    newOrder.medicines.forEach(medicine =>{

        let query="CALL insert_into_order_items(?,?)";
        let data=[res[0][0].result,medicine];

        sql.query(query,data,(err,res)=>{
          if(err)
          {
            console.log("error while inserting medicine",err);
            return;
          }
          console.log("Medicine :",medicine);
        });
    });
    newOrder.prescription.forEach(pres =>{

        let query="CALL insert_into_order_prescription(?,?)";
        let data=[res[0][0].result,pres];

        sql.query(query,data,(err,res)=>{
          if(err)
          {
            console.log("error while inserting prescription",err);
            return;
          }
          console.log("prescription :",pres);
        });
    });

    console.log("created order: ", { id: res.insertId, ...newOrder });
    result(null, { id: res.insertId, ...newOrder });
  });
};

//for getting all orders
Order.getAll = result => {
  sql.query("CALL select_all_orders()", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    //console.log("Orders: ", res);
    let flag=0;
    let  ans=[];
    let val={};
    let pid=0;
    var mi=new Set();
    var pi=new Set();
    //console.log(res);
    res[0].forEach(elem =>{
        if(flag==0)
        {
          val["order_id"]=elem["order_id"];
          val["patient_id"]=elem["patient_id"];
          val["date_of_order"]=elem["date_of_order"];
          val["total_price"]=elem["total_price"];
          val["order_status"]=elem["order_status"];
          pid=elem["order_id"];
          mi.add(elem["medicine_name"]);
          pi.add(elem["prescription_image"]);
          flag=1;
        }
        else{
          if(elem["order_id"]==pid)
          {
            mi.add(elem["medicine_name"]);
            pi.add(elem["prescription_image"]);
          }
          else
          {

            val["medicine_name"]=Array.from(mi);
            val["prescription_image"]=Array.from(pi);
            ans.push(val);
            val={};
            val["order_id"]=elem["order_id"];
            val["patient_id"]=elem["patient_id"];
            val["date_of_order"]=elem["date_of_order"];
            val["total_price"]=elem["total_price"];
            val["order_status"]=elem["order_status"];

            pid=elem["order_id"];
            mi.clear();
            pi.clear();
            mi.add(elem["medicine_name"]);
            pi.add(elem["prescription_image"]);

          }
        }
    });
     var count = Object.keys(val).length;
     if(count!=0)
     {
       val["medicine_name"]=Array.from(mi);
       val["prescription_image"]=Array.from(pi);
       ans.push(val);
     }

    console.log(ans);
    result(null, ans);
  });
};

Order.getCurrentOrdersForStore=(info, result) => {
   console.log("reached here")
   let insertionQuery = 'Call store_show_current_orders(?)';
   let data = [info.storeID];
   sql.query(insertionQuery, data, (err, res) => {
     if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
     }
     console.log("store current order res = ", res);
     result(null, res);
   });
}
Order.getPastOrdersForStore=(info, result) => {
   let insertionQuery = 'Call store_show_past_orders(?)';
   let data = [info.storeID];
   sql.query(insertionQuery, data, (err, res) => {
     if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
     }
     console.log("store past order res = ", res);
     result(null, res);
   });
}
Order.getCurrentOrdersForPatient=(info, result) => {
   let insertionQuery = 'Call patient_current_orders(?)';
   let data = [info.patientID];
   sql.query(insertionQuery, data, (err, res) => {
     if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
     }
     console.log("patient current order res = ", res);
     result(null, res);
   });
}

Order.getPastOrdersForPatient=(info, result) => {
   let insertionQuery = 'Call patient_past_orders(?)';
   let data = [info.patientID];
   sql.query(insertionQuery, data, (err, res) => {
     if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
     }
     console.log("patient past order res = ", res);
     result(null, res);
   });
}
//find order with given id
Order.findById=(id,result) =>{
  sql.query('CALL find_order(?)',[id],(err,res)=>{
    if(err)
    {
      console.log("error: ",err);
      result(err,null);
      return;
    }

    if(res.affectedRows==0)
    {
      console.log("Order id not found");
      result({ kind: "not_found" }, null);
      return;
    }
    let flag=0;
    let  ans=[];
    let val={};
    let pid=0;
    var mi=new Set();
    var pi=new Set();
    //console.log(res);
    res[0].forEach(elem =>{
        if(flag==0)
        {
          val["order_id"]=elem["order_id"];
          val["patient_id"]=elem["patient_id"];
          val["date_of_order"]=elem["date_of_order"];
          val["total_price"]=elem["total_price"];
          val["order_status"]=elem["order_status"];
          pid=elem["order_id"];
          mi.add(elem["medicine_name"]);
          pi.add(elem["prescription_image"]);
          flag=1;
        }
        else{
          if(elem["order_id"]==pid)
          {
            mi.add(elem["medicine_name"]);
            pi.add(elem["prescription_image"]);
          }
          else
          {

            val["medicine_name"]=Array.from(mi);
            val["prescription_image"]=Array.from(pi);
            ans.push(val);
            val={};
            val["order_id"]=elem["order_id"];
            val["patient_id"]=elem["patient_id"];
            val["date_of_order"]=elem["date_of_order"];
            val["total_price"]=elem["total_price"];
            val["order_status"]=elem["order_status"];

            pid=elem["order_id"];
            mi.clear();
            pi.clear();
            mi.add(elem["medicine_name"]);
            pi.add(elem["prescription_image"]);

          }
        }
    });
     var count = Object.keys(val).length;
     if(count!=0)
     {
       val["medicine_name"]=Array.from(mi);
       val["prescription_image"]=Array.from(pi);
       ans.push(val);
     }


    //console.log("Order :",res);
    result(null,ans);
  });
};

//find previous orders using patient id
Order.findPrevOrderById=(id,result) =>{
  sql.query('CALL find_prev_orders(?)',[id],(err,res)=>{
    if(err)
    {
      console.log("error: ",err);
      result(err,null);
      return;
    }

    if(res.affectedRows==0)
    {
      console.log("patient id not found");
      result({ kind: "not_found" }, null);
      return;
    }
    let flag=0;
    let  ans=[];
    let val={};
    let pid=0;
    var mi=new Set();
    var pi=new Set();
    //console.log(res);
    res[0].forEach(elem =>{
        if(flag==0)
        {
          val["order_id"]=elem["order_id"];
          val["patient_id"]=elem["patient_id"];
          val["date_of_order"]=elem["date_of_order"];
          val["total_price"]=elem["total_price"];
          val["order_status"]=elem["order_status"];
          pid=elem["order_id"];
          mi.add(elem["medicine_name"]);
          pi.add(elem["prescription_image"]);
          flag=1;
        }
        else{
          if(elem["order_id"]==pid)
          {
            mi.add(elem["medicine_name"]);
            pi.add(elem["prescription_image"]);
          }
          else
          {

            val["medicine_name"]=Array.from(mi);
            val["prescription_image"]=Array.from(pi);
            ans.push(val);
            val={};
            val["order_id"]=elem["order_id"];
            val["patient_id"]=elem["patient_id"];
            val["date_of_order"]=elem["date_of_order"];
            val["total_price"]=elem["total_price"];
            val["order_status"]=elem["order_status"];

            pid=elem["order_id"];
            mi.clear();
            pi.clear();
            mi.add(elem["medicine_name"]);
            pi.add(elem["prescription_image"]);

          }
        }
    });
     var count = Object.keys(val).length;
     if(count!=0)
     {
       val["medicine_name"]=Array.from(mi);
       val["prescription_image"]=Array.from(pi);
       ans.push(val);
     }


    //console.log("Order :",res);
    result(null,ans);
  });
};


//deleting a particular order id
Order.remove = (id, result) => {
  sql.query("CALL delete_order(?)", [id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found order with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted order with id: ", id);
    result(null, res);
  });
};


module.exports = Order;
