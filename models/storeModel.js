const sql = require("./db.js");

// constructor
const Store = function(store) {
  this.email_id = store.email_id;
  this.password = store.password;
  this.store_name = store.store_name;
  this.date_of_establishment = store.date_of_establishment;
  this.address_line_1 = store.address_line_1;
  this.address_line_2 = store.address_line_2;
  this.city = store.city;
  this.state = store.state;
  this.country = store.country;
  this.pincode = store.pincode;
};

Store.create = (newStore, result) => {
  let insertionQuery = 'Call store_registration(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
  let data = [newStore.email_id, newStore.password, newStore.store_name, newStore.date_of_establishment,
              newStore.address_line_1, newStore.address_line_2, newStore.city, newStore.state, newStore.country,
              newStore.pincode];
  sql.query(insertionQuery, data, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    result(null, { id: res[0][0]["result"]});
  });
};

Store.getAll = result => {
  sql.query("CALL get_all_stores()", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err,null);
      return;
    }

    console.log("Stores: ", res);
    result(null, res);
  });
};

Store.remove = (id, result) => {
  sql.query("CALL delete_store_detail(?)", [id], (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found store with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted store with id: ", id);
    result(null, res);
  });
};

Store.findById = (StoreId, result) => {
	sql.query("CALL find_store(?)",[StoreId], (err, res) => {
		if(err) {
			console.log("error: ", err);
      		result(err, null);
      		return;
		}

		if(res.length) {
			result(null, res[0]);
			return;
		}

		result({ kind: "not_found" }, null);
	});
};

Store.updateById = (id, store, result) => {
	sql.query(
		"CALL update_store(?,?,?,?,?,?,?,?,?)",
		[id,store.store_name,store.date_of_establishment,store.address_line_1, store.address_line_2, store.city, store.state, store.country, store.pincode],
		(err, res) => {
			if (err) {
		        console.log("error: ", err);
		        result(null, err);
		        return;
		  	}

		  	if(res.affectedRows == 0) {
		  		result({ kind: "not_found" }, null);
		  		return;
		  	}
		  	result(null, {id:id, ...store});
		}
	);
};


module.exports = Store;
