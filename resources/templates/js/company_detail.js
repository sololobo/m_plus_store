var request;
$('#loginButton').click(function(event) {
   if(request) {
      request.abort();
   }
   let email = $('#username').val()
   let password = $('#passwd').val()

   let data = {
      email_id : email,
      pwd : password
   }
   $.post("login", data)
      .done(function(result) {
         if(result["message"] == "USER NOT FOUND") {
            $('#error').text("You may be entering an invalid email")
         } else if(result["message"] == "INCORRECT PASSWORD") {
            $('#error').text("Your password is incorrect")
         } else {
            if(result["user_type"] == 1) {
               window.location.href = "/medicine_stock"
            } else {
               window.location.href = "/medic"
            }
         }
      })
})

$('#registerButton').click(function(event) {
   if(request) {
      request.abort()
   }
   var pemail_id = $('#email_id').val()
   var pstore_name = $('#store_name').val()
   var pdate_of_establishment = $('#date_of_establishment').val()
   var paddress_line_1 = $('#address_line_1').val()
   var paddress_line_2 = $('#address_line_2').val()
   var pcity = $('#city').val()
   var pstate = $('#state').val()
   var pcountry = $('#country').val()
   var ppincode = $('#pincode').val()
   var ppassword = $('#password').val()
   var ppsw_repeat = $('#psw-repeat').val()
   if(ppassword != ppsw_repeat) {
      $('#confError').text("Passwords must be same")
   } else {
      let data = {
         email_id: pemail_id,
         password: ppassword,
         store_name: pstore_name,
         date_of_establishment: pdate_of_establishment,
         address_line_1: paddress_line_1,
         address_line_2: paddress_line_2,
         city: pcity,
         state: pstate,
         country: pcountry,
         pincode: ppincode
      }
      console.log('data is', data)
      $.post("stores", data)
         .done(function(result) {
            console.log(result)
            if(result["id"] > 0) {
               alert("Registration Successful!")
               window.location.href = "/"
            } else {
               alert("Something went wrong!")
            }
         })
   }
})
