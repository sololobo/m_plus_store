var request;
$('#loginButton').click(function(event) {
   if(request) {
      request.abort();
   }
   let email = $('#username').val()
   let password = $('#passwd').val()

   let data = {
      email_id : email,
      pwd : password
   }
   $.post("login", data)
      .done(function(result) {
         if(result["message"] == "USER NOT FOUND") {
            $('#error').text("You may be entering an invalid email")
         } else if(result["message"] == "INCORRECT PASSWORD") {
            $('#error').text("Your password is incorrect")
         } else {
            if(result["user_type"] == 1) {
               window.location.href = "/medicine_stock"
            } else {
               window.location.href = "/medic"
            }
         }
      })
})

$('#registerButton').click(function(event) {
   if(request) {
      request.abort()
   }
   var firstName = $('#first_name').val()
   var lastName = $('#last_name').val()
   var email = $('#email_id').val()
   var ppassword = $('#password').val()
   var dob = $('#date_of_birth').val()
   var pgender = "MALE"
   var paddress_line_1 = $('#address_line_1').val()
   var paddress_line_2 = $('#address_line_2').val()
   var pcity = $('#city').val()
   var pstate = $('#state').val()
   var pcountry = $('#country').val()
   var ppincode = $('#pincode').val()
   var ppassword = $('#password').val()
   var ppsw_repeat = $('#psw-repeat').val()


   if(ppassword !== ppsw_repeat) {
      $('#confError').text("Passwords must be same")
   } else {
      let data = {
         email_id: email,
         pwd: ppassword,
         first_name: firstName,
   	    last_name: lastName,
   	    date_of_birth: dob,
   	    gender:pgender,
         addr_line_1: paddress_line_1,
         addr_line_2: paddress_line_2,
         city: pcity,
         state: pstate,
         country: pcountry,
         pincode: ppincode
      }
      console.log('data is', data)
      $.post("customers", data)
         .done(function(result) {
            console.log(result)
            if(result["id"] > 0) {
               alert("Registration Successful!")
               window.location.href = "/"
            } else {
               alert("Something went wrong!")
            }
         })
   }
})
