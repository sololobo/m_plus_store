//alert("js call ho rha ha")
const app = document.getElementById('main_body');

const container = document.createElement('div');
container.setAttribute('class', 'container');
container.setAttribute('id', 'container_id');

app.appendChild(container);

const search_container = document.createElement('div');
search_container.setAttribute('class', 'search_container');
search_container.setAttribute('id', 'search_container_id');

app.appendChild(search_container);
$(window).on('load', function() {
  $.get("/medicines").done(function(data,status)
  {
  	console.log("get medicines called");
    //console.log("data :",data);
    //console.log("status",status);

    data[0].forEach(medicine =>{

      const card = document.createElement('div');
      card.setAttribute('class', 'card');
      card.style.width = "250px";

      const card_img = document.createElement('img');
      card_img.setAttribute('class','card-img-top');
      card_img.src = '../images/logo.png';

      const card_body = document.createElement('div');
      card_body.setAttribute('class', 'card_body');

      const h5 = document.createElement('h5');
      h5.textContent = medicine.medicine_name;

      const manf_name = document.createElement('h6');
      manf_name.textContent = "Mnf name: "+medicine.manufacturer_name;

      const Quantity = document.createElement('h6');
      Quantity.textContent ="Quantity per pack:"+medicine.quantity_per_pack;

      const price = document.createElement('h6');
      price.textContent = "Price: "+medicine.selling_price_after_discount;

      const dis = document.createElement('h6');
      dis.textContent = "Discount: "+medicine.discount;

      // const dis_price = document.createElement('h6');
      // dis_price.textContent = "Discount-price: 25";
      const desc=document.createElement('div');
      let dval="desc"+medicine.medicine_id;
      desc.setAttribute('id',dval);

      const discription = document.createElement('button');
      discription.setAttribute('class','btn btn-primary');
      let did="did"+medicine.medicine_id;
      discription.setAttribute('id',did);
      discription.textContent = "Discription";
      discription.addEventListener("click", function(){
        document.getElementById(did).disabled=true;
        let text="Description:<br>"+"Manufacturing date:"+medicine.manufacturing_date+"<br>"+"Expiry date:"+medicine.expiry_date+"<br>"+"About:"+medicine.description+"<br>";
          document.getElementById(dval).innerHTML=text;
      });

      const cart = document.createElement('button');
      cart.setAttribute('class','btn btn-danger');
      cart.textContent = "Add to cart";
      let val="button"+medicine.medicine_id;
      cart.setAttribute('id',val);
      cart.addEventListener("click", function(){
          let data={
            medicineId: medicine.medicine_id,
            quantity: medicine.quantity_per_pack
          }

          $.post("/cart",data).done(function(result){
            if(result.length === 0 )
              alert("Failed to add medicine");
            else {
              console.log("Added");
              document.getElementById(val).innerHTML="Added";
              document.getElementById(val).disabled=true;
            }
          });
      });

          container.appendChild(card);
          card.appendChild(card_img);
          card.appendChild(card_body);
          card_body.appendChild(h5);
          card_body.appendChild(manf_name);
          card_body.appendChild(Quantity);
          card_body.appendChild(price);
          card_body.appendChild(dis);
          card_body.appendChild(desc);
          //card_body.appendChild(dis_price);
          card_body.appendChild(discription);
          card_body.appendChild(cart);

          $("#search_container_id").hide();
          $("#container_id").show();
    });
  });
});


$("#search_btn").click(function(){
  document.getElementById("search_container_id").innerHTML="";
  let val=$('#search_input').val();
  if(val.length === 0 )
  {
    alert("Blank field");
  }
  else
  {
  let url="/medicines/" + val;


  //console.log(url);
  $.get(url).done(function(data,status)
  {
    console.log("data :",data);
    //console.log("status",status);
    if(data.length==0)
      alert("not found");
    else
    {
      data.forEach(medicine =>{

      const card = document.createElement('div');
      card.setAttribute('class', 'card');
      card.style.width = "250px";

      const card_img = document.createElement('img');
      card_img.setAttribute('class','card-img-top');
      card_img.src = '../images/logo.png';

      const card_body = document.createElement('div');
      card_body.setAttribute('class', 'card_body');

      const h5 = document.createElement('h5');
      h5.textContent = medicine.medicine_name;

      const manf_name = document.createElement('h6');
      manf_name.textContent = "Mnf name: "+medicine.manufacturer_name;

      const Quantity = document.createElement('h6');
      Quantity.textContent ="Quantity per pack:"+medicine.quantity_per_pack;

      const price = document.createElement('h6');
      price.textContent = "Price: "+medicine.selling_price_after_discount;

      const dis = document.createElement('h6');
      dis.textContent = "Discount: "+medicine.discount;

      const desc=document.createElement('div');
      let dval="desc"+medicine.medicine_id;
      desc.setAttribute('id',dval);

      const discription = document.createElement('button');
      discription.setAttribute('class','btn btn-primary');
      let did="did"+medicine.medicine_id;
      discription.setAttribute('id',did);
      discription.textContent = "Discription";
      discription.addEventListener("click", function(){
        document.getElementById(did).disabled=true;
        let text="Description: \n"+"Manufacturing date:"+medicine.manufacturing_date+"\n"+"Expiry date:"+medicine.expiry_date+"\n"+medicine.description;
          document.getElementById(dval).innerHTML=text;
      });

      const cart = document.createElement('button');
      cart.setAttribute('class','btn btn-danger');
      cart.textContent = "Add to cart";
      let val="button"+medicine.medicine_id;
      cart.setAttribute('id',val);
      cart.addEventListener("click", function(){
          let data={
            medicineId: medicine.medicine_id,
            quantity: medicine.quantity_per_pack
          }

          $.post("/cart",data).done(function(result){
            if(result.length === 0 )
              alert("Failed to add medicine");
            else {
              console.log("Added");
              document.getElementById(val).innerHTML="Added";
              document.getElementById(val).disabled=true;
            }
          });
      });

          search_container.appendChild(card);
          card.appendChild(card_img);
          card.appendChild(card_body);
          card_body.appendChild(h5);
          card_body.appendChild(manf_name);
          card_body.appendChild(Quantity);
          card_body.appendChild(price);
          card_body.appendChild(dis);
          card_body.appendChild(desc);
          //card_body.appendChild(dis_price);
          card_body.appendChild(discription);
          card_body.appendChild(cart);

          $("#search_container_id").show();
          $("#container_id").hide();
    });
  }
  });
}
});


$('#medicine_full').click(function(){
  document.getElementById("search_container_id").innerHTML="";
  $("#search_container_id").hide();
  $("#container_id").show();
});
