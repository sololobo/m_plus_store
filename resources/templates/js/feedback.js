// This file has two functionalities :
// (1) It integrates the "Submit" button of feedback form to the post route into the feedback table
// (2) It integrates the "View More" button of Home Page to the Get route out of  the feedback table


// (1)
var request;
$('#Submit Feedback').click(function(event) {
   if(request) {
      request.abort();
   }
   let User_Id = $('#userid').val()
   let Name= $('#Name').val()
   let Age = $('#age').val()
   let Feedback = $('#feedback').val()

   let data = 
   {
    USER_ID = User_Id,
    NAME = Name,
    AGE = Age,
    FEEDBACK = Feedback
    }
   $.post("Feedback", data)
      .done(function(result) {
         if(result["message"] == "Successful")
          {
            $('#error').text("Feedback submitted succesfully !")
             window.location.href = "/"
                
            }
            else
            {
                $('#error').text("Ooops ! , some error occured . Try again.")
                window.location.href = "/"
                   
            }
         }
      }

// (2)

var request;
$('#View More').click(function(event) {
   if(request) {
      request.abort();
   }
   $.get("Feedback", data)
      .done(function(result) {
         if(result["message"] == "Succesful") {
            $('#error').text("Fetching all Feedbacks..")
            window.location.href = "/users_seefeedback"
         }   
         else
         {
            $('#error').text("Some internal error occured . Try again.")
            window.location.href = "/"
             
         }
      })
})

