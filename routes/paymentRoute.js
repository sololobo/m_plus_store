module.exports = (app) => {

	const payment = require("../controllers/paymentController.js");

	app.get("/payment", payment.getRequest);
}