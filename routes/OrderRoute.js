module.exports = app => {
    const orders = require("../controllers/orderController.js");

    // Create a new orders
    app.post("/orders", orders.create);

    // Retrieve all orders
    app.get("/orders", orders.findAll);

    // Retrieve a single orders with ordersId
    app.get("/orders/:IdFlag/:orderId", orders.find); //if flag=1 then it is current order otherwise previous order

    //  doubtfull
    //app.get("/orders/:patientId", orders.findprevOrder);

    // Update a orders with ordersId
    //app.put("/orders/:orderId", orders.update);

    // Delete a orders with ordersId
    app.delete("/orders/:orderId", orders.delete);

    //view all orders either by store or patient
    //same route for both of them
    app.get("/myorders", orders.viewOrders);
  };


//raw data
//{
// 	"date_of_order":"2020-05-10",
// 	"total_price":100,
// 	"isCOD":1,
// 	"medicines":[110,120,130],
// 	"prescription":["first","second","third","fourth"],
// 	"addressFlag":1,
// 	"address_line_1":"test address",
// 	"address_line_2":"test address 2",
// 	"city":"test city",
// 	"state":"test state",
// 	"country":"India",
// 	"pincode":109677
//
// }
