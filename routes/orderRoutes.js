module.exports = app => {
  const orders = require("../controllers/orderController.js");

  // Create a new store
  console.log("in order route");
  app.post("/orders", orders.create);

  // Retrieve all stores
  app.get("/orders", orders.findAll);

  // Retrieve a single store with storeId
  // app.get("/orders/:orderId", orders.findOne);

//   app.post('/path', upload.single('avatar'), function (req, res, next) {
//   // req.file is the `avatar` file
//   // req.body will hold the text fields, if there were any
// })


  // Delete a store with storeId
  app.delete("/orders/:orderId", orders.delete); //may or may not be present

  app.get("/myorders",orders.viewOrders);

};
