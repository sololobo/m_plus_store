module.exports = app => {
    const cart = require("../controllers/cartController.js");

    // Create a new cart
    app.post("/cart", cart.create);

    // Retrieve all cart

    app.get("/cart", cart.findAll);

    // Retrieve a single cart with cartId
   // app.get("/cart/:cartId", cart.findOne);

    // Update a cart with cartId
    app.put("/cart", cart.update);

    // Delete a cart with cartId
    app.delete("/cart", cart.delete);

    app.delete("/checkout", cart.deleteAll);

  };
  
