module.exports = app => {
    const patients = require("../controllers/patientController.js");
  
    // Create a new patient
    app.post("/patients", patients.create);
  
    // Retrieve all patients
    app.get("/patients", patients.findAll);
  
    // Retrieve a single patient with patientId
    app.get("/patients/:patientId", patients.findOne);
  
    // Update a patient with patientId
    app.put("/patients/:patientId", patients.update);
  
    // Delete a patient with patientId
    app.delete("/patients/:patientId", patients.delete);
  
  };
  