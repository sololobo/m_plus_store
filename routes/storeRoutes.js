module.exports = app => {
  const stores = require("../controllers/storeController.js");

  // Create a new store
  app.post("/stores", stores.create);

  // Retrieve all stores
  app.get("/stores", stores.findAll);

  // Retrieve a single store with storeId
  app.get("/stores/:storeId", stores.findOne);

  // Update a store with storeId
  app.put("/stores/:storeId", stores.update);

  // Delete a store with storeId
  app.delete("/stores/:storeId", stores.delete);

};
