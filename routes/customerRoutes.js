module.exports = app => {
	const customers = require("../controllers/customerController.js");

	app.post("/customers",customers.create);

	console.log("Customer route page, find All");
	app.get("/customers", customers.findAll);

	app.get("/customers/:customerId", customers.findOne);

	app.put("/customers/:customerId", customers.update);

	app.delete("/customers/:customerId", customers.delete);
};