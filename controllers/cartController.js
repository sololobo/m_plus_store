const Cart = require("../models/cartModel.js");
const cookieParser = require('cookie-parser');

// Create and Save a new Cart
exports.create = (req, res) => {
 // Validate request
 if (!req.body) {
  res.status(400).send({
   message: "Content can not be empty!"
});
}

let str = req.headers.cookie;
let parts = str.split('=');
let id = parseInt(parts[1]);

 // Create a Cart
 const cart = new Cart({
      patientId: id,
       medicineId: req.body.medicineId,
       quantity: req.body.quantity,
       total: req.body.total,
       subtotal: req.body.subtotal,
       tax: req.body.tax,
       shipping_charges: req.body.shipping_charges,
       grand_total: req.body.grand_total
   });

 // Save Cart in the database

 Cart.create(cart, (err, data) => {
    console.log("in cart create cart controller");
     if (err)
       res.status(500).send({
         message:
         err.message || "Some error occurred while creating the Cart."
     });
   else res.send(data);
});
};

// Retrieve all Carts from the database.
exports.findAll = (req, res) => {

let str = req.headers.cookie;
let parts = str.split('=');
let id = parseInt(parts[1]);

Cart.getAll(id, (err, data) => {
  if(err) {
    if(err.kind === 'not_found') {
      res.status(404).send({
        message:
        "Cart empty"
    });
  }   else {
      res.status(500).send({
        message:
        "Error getting cart"
    });
  }
}
else {
console.log("cookie in getAll = ", req.headers.cookie);
let str = req.headers.cookie;
let parts = str.split('=');
console.log("first part = ",parts[0]);
console.log("second part = ",parts[1]);
res.send(data);
}
});
};

// Find a single Cart with a CartId
//exports.findOne = (req, res) => {
//};

// Update a Cart identified by the CartId in the request
exports.update = (req, res) => {

  let str = req.headers.cookie;
  let parts = str.split('=');
  let id = parseInt(parts[1]);

  const cart = new Cart({
      patientId: id,
       medicineId: req.body.medicineId,
       quantity: req.body.quantity,
       total: req.body.total,
       subtotal: req.body.subtotal,
       tax: req.body.tax,
       shipping_charges: req.body.shipping_charges,
       grand_total: req.body.grand_total
   });

Cart.update(cart, (err, data) => {
  if(err) {
    if(err.kind === "not_found") {
      res.status(404).send({
        message: "Cart empty"
    });
  }
  else {
      res.status(500).send({
        message:
        err.message || "Error occured"
    });
  }
} else res.send({message: `Item updated`});
});
};

// Delete a Cart with the specified CartId in the request
exports.delete = (req, res) => {

  let str = req.headers.cookie;
  let parts = str.split('=');
  let id = parseInt(parts[1]);

  const cart = new Cart({
      patientId: id,
       medicineId: req.body.medicineId
   });
  console.log("in cart delete ");

Cart.remove(cart, (err, data) => {
  if(err) {
    if(err.kind === "not_found") {
      res.status(404).send({
        message: "Cart empty"
    });
  }
  else {
      res.status(500).send({
        message:
        "Error occurred"
    });
  }
} else res.send({message: `Item deleted`});
});
};

exports.deleteAll = (req, res) => {

  let str = req.headers.cookie;
  let parts = str.split('=');
  let id = parseInt(parts[1]);

  const cart = new Cart({
      patientId: id,
       medicineId: req.body.medicineId,
       quantity: req.body.quantity,
       total: req.body.total,
       subtotal: req.body.subtotal,
       tax: req.body.tax,
       shipping_charges: req.body.shipping_charges,
       grand_total: req.body.grand_total
   });

Cart.removeAll(cart, (err, data) => {
  if(err) {
    if(err.king === 'not_found') {
      res.status(404).send({
        message: "No cart"
    });
  }
  else {
      res.status(500).send({
        message:
        "Error occured"
    });
  }
} else res.send({message: `Cart deleted`});
});
};
