const Store = require("../models/storeModel.js");
const bcrypt = require('bcrypt');
const saltRounds = 10;
// Create and Save a new Store
exports.create = async (req, res) => {
   // Validate request
   if (!req.body) {
     res.status(400).send({
       message: "Content can not be empty!"
     });
   }
   console.log('The request body is',req.body)
   const password = req.body.password;
   const encryptedPassword = await bcrypt.hash(password, saltRounds);

   // Create a Store
   const store = new Store({
     email_id: req.body.email_id,
     password: encryptedPassword,
     store_name: req.body.store_name,
     date_of_establishment: req.body.date_of_establishment,
     address_line_1: req.body.address_line_1,
     address_line_2: req.body.address_line_2,
     city: req.body.city,
     state: req.body.state,
     country: req.body.country,
     pincode: req.body.pincode
   });

   // Save Store in the database
   Store.create(store, (err, data) => {
     if (err)
       res.status(500).send({
         message:
           err.message || "Some error occurred while creating the Store."
       });
     else {res.send(data);}
   });
};

// Retrieve all Stores from the database.
exports.findAll = (req, res) => {
   Store.getAll((err, data) => {
   if (err)
    res.status(500).send({
     message:
        err.message || "Some error occurred while retrieving stores."
    });
    else {
      res.send(data);
   }
});
};

// Find a single Store with a StoreId
exports.findOne = (req, res) => {
  Store.findById(req.params.StoreId, (err, data) => {
  if(err) {
    if(err.kind === 'not_found') {
      res.status(404).send({
        message:
        `No store found with this id ${req.params.StoreId}`
      });
    } 	else {
      res.status(500).send({
        message:
        "Error getting customer with this id" + req.params.StoreId
      });
    }
  }	else res.send(data);
});

};

// Update a Store identified by the StoreId in the request
exports.update = (req, res) => {
  if(!req.body) {
		res.status(400).send({
			message:"Empty content"
		});
	}

	Store.updateById(req.params.StoreId,new Store(req.body),(err, data) => {
		if(err) {
			if(err.kind === "not_found") {
				res.status(404).send({
					message: `No store with id ${req.params.StoreId}`
				});
			}
      else {
				res.status(500).send({
					message:
						err.message || "Error occured"
				});
			}
		}	else res.send(data);
	});


};

// Delete a Store with the specified StoreId in the request
exports.delete = (req, res) => {
  Store.remove(req.params.StoreId, (err, data) => {
		if(err) {
			if(err.kind === "not_found") {
				res.status(404).send({
					message:`No store with this id ${req.params.StoreId}`
				});
			}	else {
				res.status(500).send({
					message:
						"Error in deleting store with this id "+req.params.StoreId
				});
			}
		}	else res.send({message: `Store deleted`});
	});


};
