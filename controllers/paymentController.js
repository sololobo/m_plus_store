const checksum_lib = require('../paytm/checksum/checksum')
const port = 5000

exports.getRequest = (req, res) => {

	const params={}
	params['MID']='GtMdTh40805487958140',
	params['WEBSITE']='WEBSTAGING',
	params['CHANNEL_ID']='WEB',
	params['INDUSTRY_TYPE_ID']='Retail',
	params['ORDER_ID']=req.body.orderId,
	params['CUST_ID']=req.body.cutomerId,
	params['TXN_AMOUNT']='100',
	params['CALLBACK_URL']='http://localhost:4000/',
	params['EMAIL']='abcdef@gmail.com',
	params['MOBILE_NO']='9854543210'

	checksum_lib.genchecksum(params, 'Y2xa34yr_DsJY7cd', function(err, checksum){
		const txn_url="https://securegw-stage.paytm.in/order/process"

		let form_fields = ""
		for(x in params) {
			form_fields += "<input type='hidden' name='"+x+"' value='"+params[x]+"'/>"
		}

		form_fields += "<input type='hidden' name='CHECKSUMHASH' value='"+checksum+"'/>"

		var html = '<html><body><center>Wait! Do not refresh the page and Do not go back now!</center><form method="post" action="'+txn_url+'" name="f1">'+form_fields+'</form> <script type="text/javascript">document.f1.submit()</script></body></html>'
		res.writeHead(200, {'Content-Type':'text/html'})
		res.write(html)
		res.end()
	});

};