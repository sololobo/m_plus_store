const Customer = require("../models/customerModel.js");
const bcrypt = require('bcrypt');
const saltRounds = 10;

exports.create = async (req, res) => {
	if(!req.body) {
		res.status(400).send({
			message: "Empty content"
		});
	}

	const password = req.body.pwd;
  	const encryptedPassword = await bcrypt.hash(password, saltRounds);

  	console.log(encryptedPassword);

	const customer = new Customer({
		first_name:req.body.first_name,
	    last_name:req.body.last_name,
	    date_of_birth:req.body.date_of_birth,
	    gender:req.body.gender,
	    email_id:req.body.email_id,
	    pwd:encryptedPassword,
	    addr_line_1:req.body.addr_line_1,
	    addr_line_2:req.body.addr_line_2,
	    city:req.body.city,
	    state:req.body.state,
	    country:req.body.country,
	    pincode:req.body.pincode
	    
	});

	Customer.create(customer, (err, data) => {
		if(err)
			res.status(500).send({
				message:
					err.message || "Error occured"
			});
		else res.send(data);
	});
};

exports.findAll = (req, res) => {
	console.log("Customer controller page, findAll");
	Customer.getAll((err, data) => {
		if(err)
			res.status(500).send({
				message:
					err.message || "Error occured"
			});
		else res.send(data);
	});
};

exports.findOne = (req, res) => {
	Customer.findById(req.params.customerId, (err, data) => {
		if(err) {
			if(err.kind === 'not_found') {
				res.status(404).send({
					message:
					`No customer found with this id ${req.params.customerId}`
				});
			} 	else {
				res.status(500).send({
					message:
					"Error getting customer with this id" + req.parmas.customerId
				});
			}
		}	else res.send(data);
	});
};

exports.update = (req, res) => {

	if(!req.body) {
		res.status(400).send({
			message:"Empty content"
		});
	}

	Customer.updateById(
		req.params.customerId,
		new Customer(req.body),
		(err, data) => {
		if(err) {
			if(err.kind === "not_found") {
				res.status(404).send({
					message: `No customer with id ${req.params.customerId}`
				});
			}	else {
				res.status(500).send({
					message:
						err.message || "Error occured"
				});
			}
		}	else res.send(data);
	});
};

exports.delete = (req, res) => {
	Customer.remove(req.params.customerId, (err, data) => {
		if(err) {
			if(err.kind === "not_found") {
				res.status(404).send({
					message:`No customer with this id ${req.params.customerId}`
				});
			}	else {
				res.status(500).send({
					message:
						"Error in deleting customer with this id "+req.params.customerId
				});
			}
		}	else res.send({message: `Customer deleted`});
	});
};